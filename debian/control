Source: libcgi-application-plugin-authentication-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Damyan Ivanov <dmn@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libapache-htpasswd-perl <!nocheck>,
                     libauthen-simple-perl <!nocheck>,
                     libcgi-application-basic-plugin-bundle-perl <!nocheck>,
                     libcgi-application-perl <!nocheck>,
                     libcgi-pm-perl <!nocheck>,
                     libclass-dbi-perl <!nocheck>,
                     libclass-inspector-perl <!nocheck>,
                     libdbd-sqlite3-perl <!nocheck>,
                     libdbix-class-perl <!nocheck>,
                     libreadonly-perl <!nocheck>,
                     libtest-exception-perl <!nocheck>,
                     libtest-mockobject-perl <!nocheck>,
                     libtest-nowarnings-perl <!nocheck>,
                     libtest-regression-perl <!nocheck>,
                     libtest-taint-perl <!nocheck>,
                     libtest-warn-perl <!nocheck>,
                     libtest-without-module-perl <!nocheck>,
                     libuniversal-require-perl <!nocheck>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-plugin-authentication-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libcgi-application-plugin-authentication-perl.git
Homepage: https://metacpan.org/release/CGI-Application-Plugin-Authentication
Rules-Requires-Root: no

Package: libcgi-application-plugin-authentication-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libcgi-application-perl,
         libcgi-pm-perl,
         libuniversal-require-perl
Recommends: libcgi-application-basic-plugin-bundle-perl
Suggests: libapache-htpasswd-perl,
          libauthen-simple-perl,
          libcgi-application-extra-plugin-bundle-perl,
          libclass-dbi-perl,
          libcolor-calc-perl,
          libdbix-class-perl
Enhances: libcgi-application-extra-plugin-bundle-perl
Provides: libcgi-application-plugin-authentication-driver-cdbi-perl,
          libcgi-application-plugin-authentication-driver-dbic-perl
Description: authentication framework for CGI::Application
 CGI::Application::Plugin::Authentication adds the ability to authenticate
 users in your CGI::Application modules. The module separates authentication
 into Drivers (backend logic), Store (cookie management etc) and Display
 (formulation of the login form). The following drivers are bundled with this
 package:
 .
  - CGI::Application::Plugin::Authentication::Driver::DBIC
  - CGI::Application::Plugin::Authentication::Driver::CDBI
